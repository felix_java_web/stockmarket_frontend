import React, { Component, useRef, useEffect } from 'react';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';

class Balances extends Component {

    emptyBalance = {
        topUpValue: 0.0
    };

    constructor(props) {
        super(props);
        this.state = {
            cashBalance: 0.0,
            emptyBalance: this.emptyBalance
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        this.loadBalance();
    }


    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let emptyBalance = {...this.state.emptyBalance};
        emptyBalance.topUpValue = value;
        this.setState({emptyBalance});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const {emptyBalance} = this.state;

        await fetch('cash_balance/topup', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(emptyBalance),
        });
        this.props.history.push('/balances');
        this.loadBalance()
        this.emptyBalance.topUpValue = 0.0;
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.input !== this.state.input) {
            this.loadBalance();
        }
    }

    loadBalance() {
        // get page of items from api
        fetch(`/cash_balance/status`, {method: 'GET'})
            .then(response => response.json())
            .then(({cashBalance}) => {
                console.log("FETCHING BALANCES");
                this.setState({cashBalance});
            });
    }

    render() {
        const {cashBalance,emptyBalance} = this.state;
        const title = <h2>CurrentBalance {cashBalance}</h2>;

        return <div>
            <AppNavbar/>
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="name">Please input balance top up amount in USD</Label>
                        <Input type="text" name="topUpValue" placeholder="0" pattern="[0-9]*" required id="topUpValue" value={emptyBalance.topUpValue}
                               onChange={this.handleChange} autoComplete="topUpValue"/>
                        <Button color="primary" type="submit">Save</Button>{' '}
                    </FormGroup>
                </Form>
            </Container>
        </div>

    }
}

export default Balances;