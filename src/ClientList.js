import React, {Component, useRef, useEffect} from 'react';
import {Button, ButtonGroup, Container, Table, Form, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from './AppNavbar';
import {Link} from 'react-router-dom';

class ClientList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pager: {},
            pageOfItems: [],
            currentBalance: 0.0,
        };

    }

    componentDidMount() {
        this.loadPage();
    }

    componentDidUpdate() {
            this.loadPage();
    }

    loadPage() {
        // get page of items from api
        const params = new URLSearchParams(window.location.search);
        const page = parseInt(params.get('page')) || 1;
        if (page !== this.state.pager.currentPage) {
            fetch(`/stockshare/list?page=${page}`, {method: 'GET'})
                .then(response => response.json())
                .then(({pager, pageOfItems, currentBalance}) => {
                    this.setState({pager, pageOfItems, currentBalance});
                });
        }
    }

    render() {
        const {pager, pageOfItems, currentBalance, searchString} = this.state;

        const clientList = pageOfItems.map(pageOfItems => {
            return <tr key={pageOfItems.symbol}>
                <td style={{whiteSpace: 'nowrap'}}>{pageOfItems.description}</td>
                <td>{pageOfItems.displaySymbol}</td>
                <td>{pageOfItems.symbol}</td>
                <td>{pageOfItems.currency}</td>
                <td>{pageOfItems.type}</td>
                <td>
                    <ButtonGroup>
                        <Button size="sm" color="primary" tag={Link} to={"/balancesBuy/" + pageOfItems.symbol}>BUY {pageOfItems.symbol}</Button>
                    </ButtonGroup>
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <div className="float-right">
                        <h3>CurrentBalance {currentBalance}</h3>

                        {/*<Button color="success" tag={Link} to="/clients/new">Add Client</Button>*/}
                    </div>
                    <h3>Stock List</h3>
                    <div className="card-footer pb-0 pt-3">
                        {pager.pages && pager.pages.length &&
                        <ul className="pagination">
                            <li className={`page-item first-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                                <Link to={{search: `?page=1`}}
                                      className="page-link">First</Link>
                            </li>
                            <li className={`page-item previous-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                                <Link
                                    to={{search: `?page=${pager.currentPage - 1}`}}
                                    className="page-link">Previous</Link>
                            </li>
                            {pager.pages.map(page =>
                                <li key={page}
                                    className={`page-item number-item ${pager.currentPage === page ? 'active' : ''}`}>
                                    {pager.currentPage === page &&
                                    <Link to={{search: `?page=${page}`}}
                                          className="page-link">{page}</Link>
                                    }
                                </li>
                            )}
                            <li className={`page-item next-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                                <Link
                                    to={{search: `?page=${pager.currentPage + 1}`}}
                                    className="page-link">Next</Link>
                            </li>
                            <li className={`page-item last-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                                <Link to={{search: `?page=${pager.totalPages}`}}
                                      className="page-link">Last</Link>
                            </li>
                        </ul>
                        }
                    </div>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="30%">Name</th>
                            <th width="15%">Display Symbol</th>
                            <th width="15%">Symbol</th>
                            <th width="15%">Currency</th>
                            <th width="15%">Type</th>
                            <th width="10%">BUY</th>
                        </tr>
                        </thead>
                        <tbody>
                        {clientList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ClientList;