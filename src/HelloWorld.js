import React from 'react';
import './App.css';

const HelloWorld = () => {

    function sayHello() {
        alert('Hello, World!');
        fetch('/stockshare/list')
            .then(response => response.json())
            .then(data => console.log(data));
    }

    return (
        <button onClick={sayHello}>Click me!</button>
    );
};

export default HelloWorld;