import React, {Component, useRef, useEffect} from 'react';
import {Button, ButtonGroup, Container, Table} from 'reactstrap';
import AppNavbar from './AppNavbar';
import {Link} from 'react-router-dom';

class Shares extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ownStockList: [],
        };
    }

    componentDidMount() {
        this.loadPage();
    }


    componentDidUpdate() {
        // this.loadPage();
    }

    loadPage() {
        // get page of items from api
        fetch(`/stockshare/own`, {method: 'GET'})
            .then(response => response.json())
            .then(({ownStockList}) => {
                this.setState({ownStockList});
            });
    }

    render() {
        const {ownStockList} = this.state;

        const stockValueList = ownStockList.map(ownStock => {
            return <tr key={ownStock.stockCode}>
                <td style={{whiteSpace: 'nowrap'}}>{ownStock.stockCode}</td>
                <td>{ownStock.stockCount}</td>
                <td>{ownStock.stockValue}</td>
                {/*<td>*/}
                {/*    <ButtonGroup>*/}
                {/*        <Button size="sm" color="primary" tag={Link} to={"/clients/" + client.id}>Edit</Button>*/}
                {/*        <Button size="sm" color="danger" onClick={() => this.remove(client.id)}>Delete</Button>*/}
                {/*    </ButtonGroup>*/}
                {/*</td>*/}
            </tr>
        });

        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <h3>My Own Share List</h3>
                    <Table className="mt-4">
                        <thead>
                        <tr>
                            <th width="30%">Stock Code</th>
                            <th width="20%">Stock Count</th>
                            <th width="30%">Stock Value in USD</th>
                        </tr>
                        </thead>
                        <tbody>
                        {stockValueList}
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default Shares;