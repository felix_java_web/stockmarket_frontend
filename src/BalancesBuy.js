import React, {Component, useRef, useEffect} from 'react';
import {Button, Container, Form, Table, FormGroup, Input, Label} from 'reactstrap';
import AppNavbar from './AppNavbar';
import {Link} from 'react-router-dom';

class BalancesBuy extends Component {

    buyObject = {
        buyShareCount: 0,
        buyShareCode: this.props.match.params.shareCode,
        sharePriceToday: 0.0000
    };

    constructor(props) {
        super(props);
        this.state = {
            buyObject: this.buyObject,
            stockShare: {},
            pricePerShare: 0.0000,
            currentBalance: 0.0
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async componentDidMount() {
        this.loadBalance();
    }


    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let buyObject = {...this.state.buyObject};
        buyObject.buyShareCount = value;
        this.setState({buyObject});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const {buyObject} = this.state;
        await fetch(`/stockshare/buy?stockCode=${buyObject.buyShareCode}&stockCount=${buyObject.buyShareCount}&stockPrice=${buyObject.sharePriceToday}`, {method: 'get'});
        this.props.history.push('/shares');
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.input !== this.state.input) {
            this.loadBalance();
        }
    }

    loadBalance() {
        // get page of items from api
        fetch(`/stockshare/prebuy?stockCode=${this.props.match.params.shareCode}`, {method: 'GET'})
            .then(response => response.json())
            .then(({stockShare, pricePerShare, currentBalance}) => {
                console.log("FETCHING BALANCES");
                this.setState({stockShare, pricePerShare, currentBalance});
                this.buyObject.sharePriceToday = pricePerShare;
            });
    }

    render() {
        const {buyObject, stockShare, pricePerShare, currentBalance} = this.state;
        const title =
            <h2>Current Balance is {currentBalance}. Stock Code is {this.props.match.params.shareCode} </h2>;

        return <div>
            <AppNavbar/>
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label for="name">Please input number of Stock Shares to Buy</Label>
                        <Input type="text" name="buyShareCount" placeholder="0" pattern="[0-9]*" required
                               id="buyShareCount" value={buyObject.buyShareCount}
                               onChange={this.handleChange} autoComplete="buyShareCount"/>
                        <Button color="primary" type="submit">BUY</Button>{' '}
                    </FormGroup>
                </Form>

                <Table className="mt-4">
                    <thead>
                    <tr>
                        <th width="30%">Name</th>
                        <th width="20%">Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Current Balance</td>
                        <td>{currentBalance}</td>
                    </tr>
                    <tr>
                        <td>Share Currency</td>
                        <td>{stockShare.currency}</td>
                    </tr>
                    <tr>
                        <td>Price Per Share Today</td>
                        <td>{pricePerShare}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{stockShare.description}</td>
                    </tr>
                    <tr>
                        <td>Dysplay Symbol</td>
                        <td>{stockShare.displaySymbol}</td>
                    </tr>
                    <tr>
                        <td>Symbol</td>
                        <td>{stockShare.symbol}</td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td>{stockShare.type}</td>
                    </tr>
                    </tbody>
                </Table>

            </Container>
        </div>

    }
}

export default BalancesBuy;