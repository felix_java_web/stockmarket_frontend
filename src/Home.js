import React, { Component } from 'react';
import './App.css';
import AppNavbar from './AppNavbar';
import { Link } from 'react-router-dom';
import { Button, Container } from 'reactstrap';

class Home extends Component {
    render() {
        return (
            <div>
                <AppNavbar/>
                <Container fluid>
                    <Button color="link"><Link to="/clients">Stock Share List And Buy</Link></Button>
                    <Button color="link"><Link to="/balances">Balances</Link></Button>
                    <Button color="link"><Link to="/shares">Shares</Link></Button>
                </Container>
            </div>
        );
    }
}

export default Home;