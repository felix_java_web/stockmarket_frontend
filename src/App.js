import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ClientList from './ClientList';
import Balances from './Balances';
import Shares from './Shares';
import BalancesBuy from './BalancesBuy';

class App extends Component {
    render() {
        return (
            <Router>
                <Switch>
                    <Route path='/' exact={true} component={Home}/>
                    <Route path='/clients' exact={true} component={ClientList}/>
                    <Route path='/balances' exact={true} component={Balances}/>
                    <Route path='/shares' exact={true} component={Shares}/>
                    <Route path='/balancesBuy/:shareCode' exact={true} component={BalancesBuy}/>
                </Switch>
            </Router>
        )
    }
}

export default App;